<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="" />
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Privid Indonesia | Belajar Lebih Mudah</title>
		<link rel="icon" href="<?= ($BASE) ?>/public/img/privid.jpg" type="image/jpg">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="version" content="">
		<meta name="author" content="Smartindev Team">
		<meta name="robots" content="index,follow">
		<link rel="stylesheet" href="<?= ($BASE) ?>/public/assets/bootstrap/bootstrap.min.css">
	  	<link rel="stylesheet" href="<?= ($BASE) ?>/public/assets/fontawesome/css/all.min.css">
	  	<link rel="stylesheet" href="<?= ($BASE) ?>/public/assets/themify-icons/themify-icons.css">
	  	<link rel="stylesheet" href="<?= ($BASE) ?>/public/assets/linericon/style.css">
	  	<link rel="stylesheet" href="<?= ($BASE) ?>/public/assets/owl-carousel/owl.theme.default.min.css">
	  	<link rel="stylesheet" href="<?= ($BASE) ?>/public/assets/owl-carousel/owl.carousel.min.css">

	  <link rel="stylesheet" href="<?= ($BASE) ?>/public/css/style.css">
	</head>
	<body>
		<!-- NAVBAR-->
		<header class="header_area">
		    <div class="main_menu">
		      <nav class="navbar navbar-expand-lg navbar-light">
		        <div class="container box_1620">
		          <!-- Brand and toggle get grouped for better mobile display -->
		          <a class="navbar-brand logo_h text-white" href="">
		          	PRIVID
		          	<!-- <img src="ui/img/logo.png" alt=""> -->
		          </a>
		          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		          <!-- Collect the nav links, forms, and other content for toggling -->
		          <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
		            <ul class="nav navbar-nav menu_nav justify-content-end">
		              <li class="nav-item active"><a class="nav-link" href">Home</a></li> 
		              <li class="nav-item"><a class="nav-link" href="">Paket Juara</a></li> 
		              <li class="nav-item"><a class="nav-link" href="">Promo</a>
		              <li class="nav-item"><a class="nav-link" href="">Blog</a>
		              <li class="nav-item"><a class="nav-link" href="">Contact</a></li>
		            </ul>

		            <ul class="navbar-right">
		              <li class="nav-item">
		                <button class="button button-header bg">Ambil Penawaran</button>
		              </li>
		            </ul>
		          </div> 
		        </div>
		      </nav>
		    </div>
		</header>
		<!-- END NAVBAR -->



		<!-- FOOTER -->
		<footer class="footer-area section-gap">
			<div class="container">
				<div class="row">
					<div class="col-xl-3 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
						<h4>Ngelesin</h4>
						<ul>
							<li><a href="#">Tentang Kami</a></li>
							<li><a href="#">Karir</a></li>
							<li><a href="#">Tentang Kami</a></li>
							<li><a href="#">Blog</a></li>
							<li><a href="#">Partner Guru</a></li>
						</ul>
					</div>
					<div class="col-xl-3 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
						<h4>Layanan</h4>
						<ul>
							<li><a href="#">Topup Saldo</a></li>
							<li><a href="#">Bayar Paket Belajar</a></li>
							<!-- <li><a href="#">Investor Relations</a></li>
							<li><a href="#">Terms of Service</a></li> -->
						</ul>
					</div>
					<!-- <div class="col-xl-3 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
						<h4>Features</h4>
						<ul>
							<li><a href="#">Jobs</a></li>
							<li><a href="#">Brand Assets</a></li>
							<li><a href="#">Investor Relations</a></li>
							<li><a href="#">Terms of Service</a></li>
						</ul>
					</div> -->
					<!-- <div class="col-xl-2 col-sm-6 mb-4 mb-xl-0 single-footer-widget">
						<h4>Resources</h4>
						<ul>
							<li><a href="#">Guides</a></li>
							<li><a href="#">Research</a></li>
							<li><a href="#">Experts</a></li>
							<li><a href="#">Agencies</a></li>
						</ul>
					</div> -->
					<div class="col-xl-4 col-md-8 mb-4 mb-xl-0 single-footer-widget">
						<h4>Download Gratis Aplikasi privid</h4>
						<!-- <p>You can trust us. we only send promo offers,</p> -->
						<div class="form-wrap" id="mc_embed_signup">
							<form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
							 method="get" class="form-inline">
								<input class="form-control" name="EMAIL" placeholder="Your Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Your Email Address '"
								 required="" type="email">
								<button class="click-btn btn btn-default">subscribe</button>
								<div style="position: absolute; left: -5000px;">
									<input name="b_36c4fd991d266f23781ded980_aefe40901a" tabindex="-1" value="" type="text">
								</div>

								<div class="info"></div>
							</form>
						</div>
					</div>
				</div>
				<div class="footer-bottom row align-items-center text-center text-lg-left">
					<p class="footer-text m-0 col-lg-3 col-md-12">&copy;<script>document.write(new Date().getFullYear());</script> Privid Indonesia.All rights reserved</p>
					<p class="footer-text m-0 col-lg-3 col-md-12 text-center">
						<a href="">Kebijakan Privasi</a>
					</p>
					<p class="footer-text m-0 col-lg-3 col-md-12">
						<a href="">Syarat & Ketentuan</a>
					</p>
					<div class="col-lg-3 col-md-12 text-center text-lg-right footer-social">
						<a href="#"><i class="fab fa-facebook-f"></i></a>
						<a href="#"><i class="fab fa-twitter"></i></a>
						<a href="#"><i class="fab fa-instagram"></i></a>
						<a href="#"><i class="fab fa-youtube"></i></a>
						<a href="#"><i class="fab fa-dribbble"></i></a>
					</div>
				</div>
			</div>
		</footer>
		<!-- END FOOTER -->
		<!-- SCRIPT -->
		<script src="<?= ($BASE) ?>/public/assets/jquery/jquery-3.2.1.min.js"></script>
  		<script src="<?= ($BASE) ?>/public/assets/bootstrap/bootstrap.bundle.min.js"></script>
  		<script src="<?= ($BASE) ?>/public/assets/owl-carousel/owl.carousel.min.js"></script>
  		<script src="<?= ($BASE) ?>/public/js/jquery.ajaxchimp.min.js"></script>
  		<script src="<?= ($BASE) ?>/public/js/mail-script.js"></script>
  		<script src="<?= ($BASE) ?>/public/js/main.js"></script>
	</body>
</html>
