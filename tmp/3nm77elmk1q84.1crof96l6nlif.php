<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="" />
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<title>Privid Indonesia | Belajar Lebih Mudah</title>
		<link rel="icon" href="<?= ($BASE) ?>/public/img/privid.jpg" type="image/jpg">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="version" content="">
		<meta name="author" content="Smartindev Team">
		<meta name="robots" content="index,follow">
		<link rel="stylesheet" href="<?= ($BASE) ?>/public/assets/bootstrap/bootstrap.min.css">
	  	<link rel="stylesheet" href="<?= ($BASE) ?>/public/assets/fontawesome/css/all.min.css">
	  	<link rel="stylesheet" href="<?= ($BASE) ?>/public/assets/themify-icons/themify-icons.css">
	  	<link rel="stylesheet" href="<?= ($BASE) ?>/public/assets/linericon/style.css">
	  	<link rel="stylesheet" href="<?= ($BASE) ?>/public/assets/owl-carousel/owl.theme.default.min.css">
	  	<link rel="stylesheet" href="<?= ($BASE) ?>/public/assets/owl-carousel/owl.carousel.min.css">

	  <link rel="stylesheet" href="<?= ($BASE) ?>/public/css/style.css">
	</head>
	<body>
		<!-- NAVBAR-->
		<header class="header_area">
		    <div class="main_menu">
		      <nav class="navbar navbar-expand-lg navbar-light">
		        <div class="container box_1620">
		          <!-- Brand and toggle get grouped for better mobile display -->
		          <a class="navbar-brand logo_h text-white" href="">
		          	PRIVID
		          	<!-- <img src="ui/img/logo.png" alt=""> -->
		          </a>
		          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		            <span class="icon-bar"></span>
		          </button>
		          <!-- Collect the nav links, forms, and other content for toggling -->
		          <div class="collapse navbar-collapse offset" id="navbarSupportedContent">
		            <ul class="nav navbar-nav menu_nav justify-content-end">
		              <li class="nav-item active"><a class="nav-link" href">Home</a></li> 
		              <li class="nav-item"><a class="nav-link" href="">Paket Juara</a></li> 
		              <li class="nav-item"><a class="nav-link" href="">Promo</a>
		              <li class="nav-item"><a class="nav-link" href="">Blog</a>
		              <li class="nav-item"><a class="nav-link" href="">Contact</a></li>
		            </ul>

		            <ul class="navbar-right">
		              <li class="nav-item">
		                <button class="button button-header bg">Ambil Penawaran</button>
		              </li>
		            </ul>
		          </div> 
		        </div>
		      </nav>
		    </div>
		</header>
		<!-- END NAVBAR -->
