<?php
require 'vendor/autoload.php';

//load fatfree
$f3 = Base::instance();

//load settings
$f3->config('settings/config.ini');
$f3->config('settings/routes.ini');
$f3->set('ONERROR', function($f3){
	echo \Template::instance()->render('render/404.html');			
});

$f3->run();
?>