<?php 
/**
* 
*/
class PromoController extends BaseController
{
	
	public function promos() {
		//panggil model coba
		$cobas = new Cobas($this->db);

		//menampung data
		$this->f3->set('cobas', $cobas->find());
		$this->f3->set('view', 'pages/coba.html');
	}

	public function save() {
		$nama  = $this->f3->get('POST.nama');
		$email = $this->f3->get('POST.email');

		$cobas = new Cobas($this->db);
		$cobas->nama  = $nama;
		$cobas->email = $email;
		$cobas->save();

		$this->f3->reroute('/coba');
	}

	public function edit() {
		$cobas = new Cobas($this->db);
		$id    = $this->f3->get('PARAMS.id');

		$this->f3->set('cobaId', $cobas->load('id='.$id));
		$this->f3->set('view', 'pages/edit.html');

	}

	public function update() {
		$cobas = new Cobas($this->db);
		$id    = $this->f3->get('PARAMS.id');
		$nama  = $this->f3->get('POST.nama');
		$email = $this->f3->get('POST.email');

		$cobas->load(array("id=?", $id));
		$cobas->nama  = $nama;
		$cobas->email = $email;
		$cobas->update();

		$this->f3->reroute('/coba');
	}


	public function remove() {
		$id = $this->f3->get('PARAMS.id');
		$cobas = new Cobas($this->db);
		$cobas->load(array("id=?", $id));
		$cobas->erase();

		$this->f3->reroute('/coba');
	}
}

?>