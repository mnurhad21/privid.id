<?php

class HomeController extends BaseController
{
	
	public function home() {
		$this->f3->set('view', 'pages/index.html');
	}

	public function about() {
		$this->f3->set('view', 'pages/about.html');
	}

	public function contact() {
		$this->f3->set('view', 'pages/contact.html');
	}

	public function privasi() {
		$this->f3->set('view', 'pages/privasi.html');
	}

	public function terms() {
		$this->f3->set('view', 'pages/terms.html');
	}

	public function listBlog() {
		$this->f3->set('view', 'pages/blog.html');
	}

	public function detailBlog() {
		$this->f3->set('view', 'pages/detail-blog.html');
	}
}